// 12. Collections
//На вход подается строка с текстом. Необходимо посчитать,
// сколько встречается раз каждое слово в этой строке.
//Вывести:
//Слово - количество раз
//Слово - количество раз\
//Использовать Map, у объектов String есть метод
// для деления строки на части по определенному символу.
// Слово - символы, ограниченные пробелами справа и слева.
//Пример:
//Маша и три медведя
//Маша - 1 раз
//и - 1 раз
//три - 1 раз
//медведя - 1раз


import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        String text = "Маша и три медведя: раз медведь, два медведь, три медведь, Маша";
        String[] array = text.split(" ");
        System.out.println(Arrays.toString(array));

        HashMap<String, Integer> map = new HashMap<>();
        for (int i = 0; i < array.length; i++) {
            String word = array[i];
            if (map.containsKey(word)) {
                map.put(word,map.get(word) + 1);
            } else {
                map.put(word, 1);
            }
        }
        for(Map.Entry<String, Integer> entry : map.entrySet()){
            System.out.println("Слово " + entry.getKey() + " повторялось " + entry.getValue() + " раз ");
        }
    }
}
