package repository;

import model.User;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class FileService {

    public static List<User> readAllUser(File file) {
        List<User> users = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(file))){
            users = reader.lines().map(str->{
                String[] userInfo = str.split("\\|");
                int id = Integer.parseInt(userInfo[0]);
                int age = Integer.parseInt(userInfo[3]);
                boolean isWorker = Boolean.parseBoolean(userInfo[4]);
                User user = new User(id, userInfo[1], userInfo[2], age, isWorker );
                return user;
            }).collect(Collectors.toList());
        } catch (FileNotFoundException exception) {
            System.out.println("Файл не найден");
            throw new RuntimeException();
        } catch (IOException e) {
            System.out.println("Возникли проблемы при работе с файлом");
            e.printStackTrace();
            }
            return users;
    }

    public static void writeAllUser(List<User> users, File file) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(file ))) {
            users.stream().map(User::toString).forEach(str -> {
                try{
                    writer.write(str);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }
}
