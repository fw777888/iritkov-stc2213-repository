package repository;

import exception.UserNotFoundException;
import model.User;

import java.io.File;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

public class UsersRepositoryFileImpl implements UsersRepository {
    File file;
    public UsersRepositoryFileImpl(File file) {
        this.file = file;
    }
    

    @Override
    public User findById(int id) throws UserNotFoundException {
        List<User> users = FileService.readAllUser(file);
        for (User user:users) {
            if (user.getId() == id){
                return user;
            }
        }
        throw new UserNotFoundException();
//        User findUser = users.stream().filter(u -> u.getId() == id).findFirst().orElse(null);
//
//        if (findUser != null){
//            return findUser;
//        } else {
//            throw new UserNotFoundException();
//        }
    }

    @Override
    public void create(User newUser) {
        List<User> users = FileService.readAllUser(file);
        users.add(newUser);
        FileService.writeAllUser(users, file);
    }

    @Override
    public boolean update(User updateUser) {
        List<User> users = FileService.readAllUser(file);
        List<User> newUserList = users.stream().map(u->{
            if (u.getId() == updateUser.getId()) {
                u = updateUser;
            }
            return u;
        }).collect(Collectors.toList());
//        for (User user:users) {
//            if (user.getId() == updateUser.getId()){
//                user.setAge(updateUser.getAge());
//            }
//        }
        FileService.writeAllUser(newUserList, file);
        return true;
    }

    @Override
    public void delete(int id) {
        List<User> users = FileService.readAllUser(file);
        //for (User user : users) {

        //}
        // пройтись по юзерам итератором и с помощью iterate.remove()
        // через  Stream filter
        FileService.writeAllUser(users, file);
    }
}
