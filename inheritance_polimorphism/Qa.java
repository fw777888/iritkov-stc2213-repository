package inheritance_polimorphism;

public class Qa extends Worker{
    private String program;

    Qa() {
        super();
    }

    public Qa(String name, String lastName, String profession) {
        super(name, lastName, profession);
    }
    public void goToWork(){
        super.goToWork();
        System.out.println(" Тестирую ");
    }
}
