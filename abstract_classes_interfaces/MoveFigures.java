package abstract_classes_interfaces;

public class MoveFigures {
    public static void main(String[] args) {
        Moveable[] moveableFigures = new Moveable[2];
        moveableFigures[0] = new Circle(3, 10, 10);
        moveableFigures[1] = new Square(5, 2, 2);

        for (Moveable moveable : moveableFigures){
            moveable.move(5, 10);
            moveable.printCurrenntCoordinates();
            moveable.moveNewCoord(0, 0);
            moveable.printCurrenntCoordinates();
        }
    }
}
