package abstract_classes_interfaces;

public class Ellipse extends Figure{
    private final double  PI = 3.14;
    private  int radius1;
    private  int radius2;

    public Ellipse(int radius1, int radius2) {
        this.radius1 = radius1;
        this.radius2 = radius2;
    }

    public Ellipse(int radius1, int radius2, int x, int y) {
        super(x, y);
        this.radius1 = radius1;
        this.radius2 = radius2;
    }

    public Ellipse(){
    }

    @Override
    public double getPerimeter() {
        return 2*PI*Math.pow(((Math.pow(radius1, 2) +Math.pow(radius2, 2))/2), 0.5);
    }

    @Override
    public String getDescription() {
        return "Ellipse";
    }

}
