package inheritance_polimorphism;

public class main {
    public static void main(String[] args) {
        DevOps devOps = new DevOps(
                "Ivan ", "Petrov ", "devOps ");
        devOps.goToWork();
        devOps.goToVacation(14);
        System.out.println("***************************");

        Programmer programmer = new Programmer(
                "Vasiliy ", "Ivanov ", "Programmer ");
        programmer.goToWork();
        programmer.goToVacation(15);
        System.out.println("***************************");

        Qa qa = new Qa(
                "Mike ", "Stevens ", "Qa ");
        qa.goToWork();
        qa.goToVacation(16);
        System.out.println("***************************");

        SystemAdmin systemAdmin = new SystemAdmin("Daniel ", "Defo ", "System Administrator ");
        systemAdmin.goToWork();
        systemAdmin.goToVacation(17);
        System.out.println("***************************");
    }
}
