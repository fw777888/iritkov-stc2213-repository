package abstract_classes_interfaces;

public interface Moveable {
    void moveNewCoord(int x, int y);
    void move(int x_dif, int y_dif);

    void printCurrenntCoordinates();
}
