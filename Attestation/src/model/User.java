package model;

public class User {
    int id;
    String name;
    String lastName;
    int age;
    boolean isWork;

    public User(int id, String name, String lastName, int age, boolean isWork) {
        this.id = id;
        this.name = name;
        this.lastName = lastName;
        this.age = age;
        this.isWork = isWork;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean isWork() {
        return isWork;
    }

    public void setWork(boolean work) {
        isWork = work;
    }

    @Override
    public String toString() {
        return id + "|" + name + "|" + lastName + "|" + age + "|" + isWork + "\n";
    }
}
