//6. Алгоритмы
//На вход подается последовательность чисел, стремящаяся к бесконечности
// (последовательность может быть и из 100 чисел, может и их 1000000 чисел быть),
// оканчивающаяся на -101.
//Необходимо вывести число, которое присутствует в последовательности
// минимальное количество раз.
//Условия задачи:
//Все числа в диапазоне от -100 до 100.
//Числа встречаются не более 2 147 483 647-раз каждое.
//Сложность алгоритма - O(n)

 package algorithmsHomework;

import java.util.Random;

public class AlgorithmsHomework {
    public static void main(String[] args) {
        int [] arrayNumberRepeatCount = new int[201];
        int[]array = generateArray();
        generateArray(arrayNumberRepeatCount, array);
        int index = findMinInArray(arrayNumberRepeatCount);
        int number = index -100;
        System.out.println("число, которое присутствует в последовательности минимальное количество раз : " + number );
    }
    private static int findMinInArray(int[] arrayNumberRepeatCount) {
        int indexMinElement = -1;
        int min = 2147483647;
        for (int i = 0; i < arrayNumberRepeatCount.length; i++) {
            if(arrayNumberRepeatCount[i] < min && arrayNumberRepeatCount[i] !=0) {
                min = arrayNumberRepeatCount[i];
                indexMinElement = i;
            }
        }
        return indexMinElement;
    }
    static void generateArray(int[] arrayNumberRepeatCount, int[] array) {
        for (int i = 0; i < array.length; i++) {
            int index = array[i] + 100;
            arrayNumberRepeatCount[index] +=1;
        }
        System.out.println("Обработанный массив : ");
        printArray(arrayNumberRepeatCount);
    }
    static int[] generateArray() {
        System.out.println(" Последовательность : ");
        Random random = new Random();
        int[] array = new int[random.nextInt(200)];
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(200) - 100;
        }
        printArray(array);
        return  array;
    }

    private static void printArray(int[] array) {
        System.out.print("[");
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
        System.out.println("]");
    }

}