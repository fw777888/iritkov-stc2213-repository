package inheritance_polimorphism;

public class Programmer extends Worker{
    private String program;

    Programmer() {
        super();
    }

    public Programmer(String name, String lastName, String profession ) {
        super(name, lastName, profession);
    }
    public void goToWork(){
        super.goToWork();
        System.out.println(" пишу код ");
    }
}
