//11. Object and String
//Создать класс Human у которого будут поля:
//
//private String name;
//private String lastName;
//private String patronymic;
//private String city;
//private String street;
//private String house;
//private String flat;
//private String numberPassport;
//
//Переопределить три метода: toString(), hashCode(), equals
//Метод toString должен выводить информацию таким образом (пример):
//Пупкин Вася Варфаламеевич
//Паспорт:
//Серия: 98 22 Номер: 897643
//Город Джава, ул. Программистов, дом 15, квартира 54
//Метод equals() должен сравнивать людей по номеру паспорта

public class Main {

    public static void main(String[] args) {

        Human human1 = new Human();

        human1.setName("John");
        human1.setLastName("Smith");
        human1.setPatronymic("Bat'kovich");
        human1.setNumberPassport("0922390903");
        human1.setCity("Zurich");
        human1.setStreet("Schonleinstrasse");
        human1.setHouse("8");
        human1.setFlat("3");

        Human human2 = new Human();

        human2.setName("Joe");
        human2.setLastName("Simpson");
        human2.setPatronymic("Natanovich");
        human2.setNumberPassport("0922390903");
        human2.setCity("Manchester");
        human2.setStreet("Union");
        human2.setHouse("78");
        human2.setFlat("43");

        System.out.println(human1.toString());
        System.out.println(human2.toString());
        System.out.println(human1.equals(human2) ?
                    " номера паспортов совпадают " :
                " номера паспортов различны ");
    }

}
