package inheritance_polimorphism;

public class SystemAdmin extends Worker{
    private String program;

    SystemAdmin() {
        super();
    }

    public SystemAdmin(String name, String lastName, String profession) {
        super(name, lastName, profession);
    }
    public void goToWork(){
        super.goToWork();
        System.out.println(" Администрурую системы ");
    }
}
