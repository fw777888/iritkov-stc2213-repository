//7. ООП, Инкапсуляция
//Создать класс Human используя инкапсуляцию (модификаторы и методы доступа),
// у которого будут поля:
//String name
//String lastName
//int age
//Будет два конструктора - один пустой, второй - полный (инициализирует все поля)
//Создать класс Main, в котором будет создаваться массив на случайное количество человек.
// После инициализации массива - заполнить его объектами класса Human
// (у каждого объекта случайное значение поля age).
// После этого - отсортировать массив по возрасту и вывести результат в консоль

package homeworkIncapsulation;

import java.util.Random;

public class HomeworkIncapsulation {
    public static void main(String[] args) {

        Random randomizer = new Random();
        int humansCount = randomizer.nextInt(20);
        System.out.println("количество элементов в массиве : " + humansCount);
        Human[] humans = new Human[humansCount];
        for (int i = 0; i < humansCount; i++) {
            Human humanoid = new Human(" Name " + (i + 1), " Last  Name " + (i + 1), randomizer.nextInt(100));
            humans[i] = humanoid;
        }
        System.out.println("********************************");
        System.out.println("До сортировки по возрасту : ");
        for (Human human : humans) {
            human.printInfo();
        }
        int min, minAge;
        for (int i = 0; i < humans.length; i++) {
            min = humans[i].getAge();
            minAge = i;
            for (int j = i + 1; j < humans.length; j++) {
                if (min > humans[j].getAge()) {
                    min = humans[j].getAge();
                    minAge = j;
                }
            }
            Human temp = humans[i];
            humans[i] = humans[minAge];
            humans[minAge] = temp;
        }
        System.out.println("********************************");
        System.out.println("После сортировки по возрасту : ");
        for (Human human : humans) {
            human.printInfo();
        }
    }
}

