// 10. Анонимные классы и лямбда выражения
//Предусмотреть функциональный интерфейс:
//interface ByCondition {
// boolean isOk(int number);
//}
//Написать класс Sequence, в котором должен присутствовать метод filter:
//public static int[] filter(int[] array, ByCondition condition) {
//...
//}
//Данный метод возвращает массив, который содержит элементы,
// удовлетворяющиие логическому выражению в condition.
//В main в качестве condition подставить:
//проверку на четность элемента
//проверку, является ли сумма цифр элемента четным числом.
//Доп. задача: проверка на четность всех цифр числа. (число состоит из цифр)
//Доп. задача: проверка на палиндромность числа
// (число читается одинаково и слева, и справа -> 11 - палиндром, 12 - нет,
// 3333 - палиндром, 3211 - нет, и т.д.).

public class Main {

    public static void main(String[] args) {
      int[] someArray = {10, 34, 5, 783, 7, 45, 76};

      // проверка на четность элемента массива
      ByCondition byCondition1 = new ByCondition() {
          @Override
          public boolean isOk(int number) {
              return (number % 2 == 0);
          }
      };
        System.out.println(Arrays.toString(Sequence.filter(someArray, byCondition1)));

      // проверка является ли сумма цифр элемента четным числом
        ByCondition byCondition2 = new ByCondition() {
            @Override
            public boolean isOk(int number) {
                int sum = 0;
                while (sum %2 == 0) {
                    sum += number % 10;
                    number /= 10;
                }

                return false;
            }
        };
// проверка на четность всех цифр числа
      ByCondition byCondition3 = new ByCondition() {
          @Override
          public boolean isOk(int number) {
              while (number != 0) {
                  int lastDigit = number %10;
                  if (lastDigit %2 == 0)
                      number /= 10;
                  else return false;
              }
              return true;
          }
      };

    }
}
