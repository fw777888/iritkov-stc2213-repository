package repository;

import exception.UserNotFoundException;
import model.User;


public interface UsersRepository {
    User findById(int id) throws UserNotFoundException; // - поиск юзера по ID
    void create(User user); // - создание нового юзера (запись в файл)
    boolean update(User user); // - обновление информации по юзеру
    void delete(int id); // - удаление юзера по ID
}
