package abstract_classes_interfaces;

public class Square extends Rectangle implements Moveable{
    Square(int a) {
        super(a, a);
    }

    Square(int a, int x, int y){
        super(a, a, x, y);
    }

    @Override
    public void moveNewCoord(int x, int y) {
        setX(x);
        setY(y);
    }

    @Override
    public void move(int x, int y) {
        this.setX(this.getX() + x);
        this.setY(this.getY() + y);
    }
    @Override
    public String getDescription() {
        return "Square ";
    }

    @Override
    public void printCurrenntCoordinates() {
        System.out.println("координаты одной точки квадрата X : " + getX() + " Y : " + getY());
    }
}
