package abstract_classes_interfaces;

public class Main {
    public static void main(String[] args) {
        Figure[] figures = new Figure[4];
        figures[0] = new Ellipse(2, 6);
        figures[1] = new Rectangle(5, 3);
        figures[2] = new Square(6);
        figures[3] = new Circle(4);

        for (Figure figure : figures) {
            System.out.println(
                    "Периметр фигуры " + figure.getDescription() + " = " + figure.getPerimeter());
        }
    }
}
