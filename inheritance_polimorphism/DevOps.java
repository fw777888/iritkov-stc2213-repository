package inheritance_polimorphism;

public class DevOps extends Worker {

    private String program;

    DevOps() {
        super();
    }

    public DevOps(String name, String lastName, String profession) {
        super(name, lastName, profession);
    }
    public void goToWork(){

        super.goToWork();
        System.out.println(" выполняю работу DevOps ");
    }
}
