package abstract_classes_interfaces;

public class Rectangle extends Figure{
    private int a;
    private int b;

    public Rectangle(int a, int b) {
        this.a = a;
        this.b = b;
    }

    public Rectangle(int a, int b, int x , int y){
        super(x, y);
        this.a = a;
        this.b = b;
    }
    public Rectangle(){
    }
    @Override
    public  double getPerimeter() {
        return 2*(this.a + this.b);
    }

    @Override
    public String getDescription() {
        return "Rectangle";
    }

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public int getB() {
        return b;
    }

    public void setB(int b) {
        this.b = b;
    }
}
