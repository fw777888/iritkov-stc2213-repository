import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
    int a, min;
    Scanner sc = new Scanner(System.in);
    a = sc.nextInt();
    min = a;
    while (a != -1) {
        if(a < min) {
            min = a;
        }
        a = sc.nextInt();
    }
        System.out.println(min);
    }
}
