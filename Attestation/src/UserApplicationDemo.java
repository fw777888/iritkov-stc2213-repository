import model.User;
import exception.UserNotFoundException;
import repository.UsersRepository;
import repository.UsersRepositoryFileImpl;

import java.io.File;
import java.util.Scanner;

public class UserApplicationDemo {
    public static void main(String[] args) {

        String filename = "input.txt";
        File inputFile = new File("resources", filename);
        UsersRepository urfi = new UsersRepositoryFileImpl(inputFile);
try {
            User user = urfi.findById(1);
            System.out.println("Найден человек - " + user);

            user.setAge(85);
            user.setName(user.getName() + "Konrad");

            if (urfi.update(user)) {
                System.out.println("Данные сохранены");
            } else {
                System.out.println("что-то пошло не так");
            }

            /*User newUser = new User();
            urfi.create(newUser);*/

            urfi.delete(8);

        } catch (UserNotFoundException e) {
            System.out.println("Человека с таким идентификатором не существует");
        }
    }
}
