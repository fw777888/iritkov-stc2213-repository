package abstract_classes_interfaces;

public class Circle extends Ellipse implements Moveable{
    public Circle(int radius1) {
        super(radius1,radius1);
    }

    public Circle(int radius1, int x, int y){
        super(radius1, radius1, x, y);
    }
    @Override
    public void moveNewCoord(int x, int y) {
        setX(x);
        setY(y);
    }

    @Override
    public void move(int x, int y) {
        this.setX(this.getX() + x);
        this.setY(this.getY() + y);
    }

    @Override
    public String getDescription() {
        return "Circle ";
    }

    @Override
    public void printCurrenntCoordinates() {
        System.out.println("координаты центра круга X : " + getX() + " Y : " + getY());
    }
}
